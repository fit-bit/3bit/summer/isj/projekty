#!/usr/bin/env python3

"""
    Funkce slouží jako dekorátor a umožňuje logovat volání funkcí do předpřipravení struktury. Dekorátor má dva paremetry key - klíč do struktury Counter a counts - samotná struktura Counter.
    Pokud není klíč zadaný, použije se jako klíč jméno funkce.

    Funkce do struktury counter ukláda kolikrát se funkce s daným klíčem zavolala.
    Při samotném zavolání se vypíše jméno funkce s parametry.
"""
def log_and_count(*decorator_args, **decorator_kwargs):
    def decorator(func):
        def func_invoke(*args, **kwargs):
            # Získání argumentů dekorátoru
            key = decorator_kwargs.get('key', "")
            counts = decorator_kwargs.get('counts', None) # TODO Nějaké ověření ¯\_(ツ)_/¯

            # Vytvoření záznamu o zavolání funkce do struktury
            # Pokud nebyl při inicializaci dodaný klíč, tak se použije jméno volané funkce
         
            if (key == ""):
                key = func.__name__

            counts[key] += 1
            # Výpis o zavolání funkce s jejímy parametry
            print("called %s with %s and %s"%(func.__name__, args, kwargs))

            return func(*args, **kwargs)
        return func_invoke
    return decorator

import collections

my_counter = collections.Counter()

@log_and_count(key = 'basic functions', counts = my_counter)
def f1(a, b=2):
    return a ** b

@log_and_count(key = 'basic functions', counts = my_counter)
def f2(a, b=3):
    return a ** 2 + b

@log_and_count(counts = my_counter)
def f3(a, b=5):
    return a ** 3 - b

f1(2)
f2(2, b=4)
f1(a=2, b=4)
f2(4)
f2(5)
f3(5)
f3(5,4)

"""
vypíše postupně:
called f1 with (2,) and {}
called f2 with (2,) and {'b': 4}
called f1 with () and {'a': 2, 'b': 4}
called f2 with (4,) and {}
called f2 with (5,) and {}
called f3 with (5,) and {}
called f3 with (5, 4) and {}
"""

print(my_counter)
"""
Counter({'basic function': 5, 'f3': 2})
"""

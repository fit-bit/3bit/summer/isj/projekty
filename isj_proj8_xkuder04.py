#!/usr/bin/env python3

"""
Generátorová funkce first_with_given_key, má 2 parametry:
 - povinný parametr iterable, odpovídající předanému iterovatelnému objektu (může být i nekonečný), 
 - nepovinný parametr key, odpovídající funkci, která při volání na položce objektu iterable vrátí hodnotu klíče, s defaultní hodnotou identické funkce (tedy vrácení přímo položky, na které je funkce zavolána).
 
Funkce aplikuje klíč na položky objektu iterable a vybírá (generuje) pouze ty, jejichž klíč se dosud nevyskytl.
"""
def first_with_given_key(iterable, key=lambda x: x):
    taken_keys = set()
    iterator = iter(iterable)
    while True:
        try:
            next_value = next(iterator)
            if key(next_value) not in taken_keys:
                yield next_value
                taken_keys.add(key(next_value))
        except StopIteration:
            break

# Příklad funkčnosti: print(tuple(first_with_given_key([[1],[2,3],[4],[5,6,7],[8,9]], key = len))) vypíše ([1], [2, 3], [5, 6, 7]).
# print(tuple(first_with_given_key([[1],[2,3],[4],[5,6,7],[8,9]], key = len)))

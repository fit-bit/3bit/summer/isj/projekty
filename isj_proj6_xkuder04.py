#!/usr/bin/env python3

class Polynomial:
    """
    Třída Polynomial pracuje s polynomy reprezentovanými pomocí seznamu, například 2x^3 - 3x + 1 bude reprezentováno jako [1,-3,0,2]

    Instance třídy je možné vytvářet několika různými způsoby:
    pol1 = Polynomial([1,-3,0,2])
    pol2 = Polynomial(1,-3,0,2)
    pol3 = Polynomial(x0=1,x3=2,x1=-3)

    Volání funkce print() vypíše polynom v obvyklém formátu:
    >>> print(pol2)
    2x^3 - 3x + 1

    Je možné vektory porovnávat:
    >>> pol1 == pol2
    True

    Polynomy je možné sčítat a umocňovat nezápornými celými čísly:
    >>> print(Polynomial(1,-3,0,2) + Polynomial(0, 2, 1))
    2x^3 + x^2 - x + 1
    >>> print(Polynomial(-1, 1) ** 2)
    x^2 - 2x + 1

    A jsou podporovány metody derivative() - derivace a at_value() - hodnota polynomu pro zadané x - obě pouze vrací výsledek, nemění samotný polynom:
    >>> print(pol1.derivative())
    6x^2 - 3
    >>> print(pol1.at_value(2))
    11
    >>> print(pol1.at_value(2,3))
    35
    """
    
    def __init__(self, *args, **kwargs):
        """ Inicializace polynomu """

        # Projití různých možností inicializace
        if (len(args) == 0 and len(kwargs) == 0):
            # Pokud není níc zadáno tak se uloží polynom 0
            self.polynom_list = [0]            
        elif (len(args) > 0 and len(kwargs) > 0):
            # Inicializace polinomu dovoluje jen zádání pomocí nepojmenovaných parametrů a nebo pojmenovaných parametrů, naráz je zadat nejde
            raise ValueError('Invalid inicialization')
        else:
            if (len(args) > 0):
                if (isinstance(args[0], list)):
                    # Zadání pomocí listu stačí jen překopírovat
                    self.polynom_list = args[0]
                else:
                    # Pokud jsou v args hodnoty samostatně, tak se jedná o zadání pomocí nepojmenovaných parametrů
                    self.polynom_list = list(args)
            else:
                # Pokud je zadaný parametr správného jména xi kde i je pořadové číslo v polynomu tak se najde parametr s nejviším i a postupně se doplní rozvoj tohoto polinomu ze získaných parametrů, pokud bude nějaká nižší hodnotahodnota mocnitele chybět, doplní se nulou 
                polynom_list = []
                polynom_keys = []
        
                #Získání platných klíčů a vybrání toho s největší hodnotou
                for key in kwargs:
                    if (key[0] == 'x' and key[1].isdigit()):
                        polynom_keys.append(key)
                
                # Pokud žádný parametr nemá očekávaný klíč, tak je vyvolána vyjímka špatné inicializace
                if (len(polynom_keys) == 0):
                    raise ValueError('Invalid inicialization') 
                    
                max_key_number = int(max(polynom_keys)[1])
                
                # Projití všech elementů polynum zadaných v parametrech a vytvoření kompletního polinomu
                for i in range(0, max_key_number+1):
                    element = kwargs.get("x%s"%i, None)
                    if element is not None:
                        polynom_list.append(element)
                    else:
                        polynom_list.append(0)

                self.polynom_list = polynom_list
        
    def __str__(self):
        """ Vypsání polynomu v matematické podobě """

        mathematical_string = ""
        first = True

        # Cyklus projde list polynomu pozpátku pro správné pořadá výpisu
        for i in range(len(self.polynom_list) -1, -1, -1):
            element = ""
            
            if (first):
                # Pokud je prvek první tak je potřeba vypsat jeho znak jen když je záporný
                if (self.polynom_list[i] < 0):
                    element = element + "-"
                    element = element + " "

                if (self.polynom_list[i] != 0):
                    # Pokud je první prvek 0, tak se počítá další prvek jako první
                    first = False   
            
            else:
                element = element + " "
                # ostatní prvky dostanou znak vždycky
                if (self.polynom_list[i] < 0):
                    element = element + "-"
                else:
                    element = element + "+"
                element = element + " "
            
            
            # Kompletní prvek se přiřadí do výstupu jen když nemá nulovou hodnotu
            if (self.polynom_list[i] != 0):
                # Pro prvek s indexem 0 se vynechá část x^i, a vypisuje se každá hodnota
                if (i == 0):
                    element = element + "%s"%(abs(self.polynom_list[i]))
                else:
                    # U ostatních prvků se vypisuje vše kromě jedničky
                    if (self.polynom_list[i] != 1 and self.polynom_list[i] != -1):
                        element = element + "%s"%(abs(self.polynom_list[i]))
                    
                    # Pro prvek s indexem 1 se vypisuje jen x
                    if (i == 1):
                        element = element + "x" 
                    else:
                        element = element + "x^%s"%(i)
                
                mathematical_string = mathematical_string + element

        # Pokud vyšlo že je string prázdný, tak se vypíše 0
        if (mathematical_string == ""):
            mathematical_string = "0"

        return mathematical_string

    def __eq__(self, other): 
        """ Porovnání dvou polynomů na rovnost """

        if not isinstance(other, Polynomial):
            # Oba objekty musí být stejného typu
            return NotImplemented

        # Dorovnáná polynomů na stejnou délku
        polynome_one = self.polynom_list
        polynome_two = other.polynom_list

        if (len(polynome_one) > len(polynome_two)):
            polynome_two = polynome_two + [0]*(len(polynome_one) - len(polynome_two))
        else:
            polynome_one = polynome_one + [0]*(len(polynome_two) - len(polynome_one))

        return polynome_one == polynome_two

    def __add__(self, other): 
        """ Sečtění dvou polynomů """

        if not isinstance(other, Polynomial):
            # Oba objekty musí být stejného typu
            return NotImplemented

        polynome_one = self.polynom_list
        polynome_two = other.polynom_list
    
        # Upravení polynomů na stejnou velikost
        if (len(polynome_one) > len(polynome_two)):
            polynome_two = polynome_two + [0]*(len(polynome_one) - len(polynome_two))
        else:
            polynome_one = polynome_one + [0]*(len(polynome_two) - len(polynome_one))

        # Sečtení polynomů
        result = [polynome_one[x] + polynome_two[x] for x in range(len(polynome_two))]  

        return Polynomial(result)

    def __pow__(self, other):
        """ Mocnina polynomu """

        # Polynom lze umocňovat jen celímy kladnímy čísli
        if (not isinstance(other, int)) or (other <= 0):
            return NotImplemented

        # hlavní cyklus který postupně vytvoří požadovaný stupěň mocniny
        # mocnina na 0 se rovná 1
        result = [1]
        for n in range(other):
            # V každém stupni mocniny se vždy přinásobý další polynom
            x = [0]*(len(self.polynom_list) + len(result)-1)

            for i in range(len(self.polynom_list)):
                ai = self.polynom_list[i]
                for j in range(len(result)):
                    x[i + j] += ai * result[j]
            result = x

        return Polynomial(result)

    def derivative(self):
        """ Derivace polynomu """

        return Polynomial([self.polynom_list[i] * i for i in range(1, len(self.polynom_list))])

    def at_value(self, x, x2 = 0):
        """ 
            hodnota polynomu pro zadané x
            pokud jsou zadány 2 hodnoty, je výsledkem rozdíl mezi hodnotou at_value() druhého a prvního parametru - může sloužit pro výpočet určitého integrálu
        """
        
        # Sečtení jednotlivých prvků s dosazeným x
        total = 0
        for i in range(0, len(self.polynom_list)):
            total = total + self.polynom_list[i]*x**i

        if (x2 != 0):
            # Výpočet hodnoty pro obě se udělá odečtením výsledku prvního x od výdledku druhého x
            total2 = 0
            for i in range(0, len(self.polynom_list)):
                total2 = total2 + self.polynom_list[i]*x2**i
            
            total = total2 - total

        return total

def test():
    assert str(Polynomial(0,1,0,-1,4,-2,0,1,3,0)) == "3x^8 + x^7 - 2x^5 + 4x^4 - x^3 + x"
    assert str(Polynomial([-5,1,0,-1,4,-2,0,1,3,0])) == "3x^8 + x^7 - 2x^5 + 4x^4 - x^3 + x - 5"
    assert str(Polynomial(x7=1, x4=4, x8=3, x9=0, x0=0, x5=-2, x3= -1, x1=1)) == "3x^8 + x^7 - 2x^5 + 4x^4 - x^3 + x"
    assert str(Polynomial(x2=0)) == "0"
    assert str(Polynomial(x0=0)) == "0"
    assert Polynomial(x0=2, x1=0, x3=0, x2=3) == Polynomial(2,0,3)
    assert Polynomial(x2=0) == Polynomial(x0=0)
    assert str(Polynomial(x0=1)+Polynomial(x1=1)) == "x + 1"
    assert str(Polynomial([-1,1,1,0])+Polynomial(1,-1,1)) == "2x^2"
    pol1 = Polynomial(x2=3, x0=1)
    pol2 = Polynomial(x1=1, x3=0)
    assert str(pol1+pol2) == "3x^2 + x + 1"
    assert str(pol1+pol2) == "3x^2 + x + 1"
    assert str(Polynomial(x0=-1,x1=1)**1) == "x - 1"
    assert str(Polynomial(x0=-1,x1=1)**2) == "x^2 - 2x + 1"
    pol3 = Polynomial(x0=-1,x1=1)
    assert str(pol3**4) == "x^4 - 4x^3 + 6x^2 - 4x + 1"
    assert str(pol3**4) == "x^4 - 4x^3 + 6x^2 - 4x + 1"
    assert str(Polynomial(x0=2).derivative()) == "0"
    assert str(Polynomial(x3=2,x1=3,x0=2).derivative()) == "6x^2 + 3"
    assert str(Polynomial(x3=2,x1=3,x0=2).derivative().derivative()) == "12x"
    pol4 = Polynomial(x3=2,x1=3,x0=2)
    assert str(pol4.derivative()) == "6x^2 + 3"
    assert str(pol4.derivative()) == "6x^2 + 3"
    assert Polynomial(-2,3,4,-5).at_value(0) == -2
    assert Polynomial(x2=3, x0=-1, x1=-2).at_value(3) == 20
    assert Polynomial(x2=3, x0=-1, x1=-2).at_value(3,5) == 44
    pol5 = Polynomial([1,0,-2])
    assert pol5.at_value(-2.4) == -10.52
    assert pol5.at_value(-2.4) == -10.52
    assert pol5.at_value(-1,3.6) == -23.92
    assert pol5.at_value(-1,3.6) == -23.92

if __name__ == '__main__':
    import doctest
    pol1 = Polynomial([1,-3,0,2])
    pol2 = Polynomial(1,-3,0,2)
    pol3 = Polynomial(x0=1,x3=2,x1=-3)
    doctest.testmod()
    test()
